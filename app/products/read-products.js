$(document).ready(function(){

    // show list of product on first load
    showProducts();
 	// when a 'read products' button was clicked
	$(document).on('click', '.read-products-button', function(){
	    showProducts();
	});
});
 
// function to show list of products
function showProducts(){

   
    $.getJSON("http://new.jr/demo/api/product/read.php", function(data){
       
        // html for listing products from readTemplate in product.js
        readProductsTemplate(data,"");
       
        //change page title
        changePageTitle("Read Products");

    });

}