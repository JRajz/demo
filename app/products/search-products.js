$(document).ready(function () {
	// body...
	// when a 'search products' button was clicked

	$(document).on('submit','#search-product-form', function(){
		//get search keywords
		var keywords = $(this).find(':input[name=keywords]').val();

		$.getJSON("http://new.jr/demo/api/product/search.php?s="+keywords, function(data){
			//template in product.js
			readProductsTemplate(data, keywords);
			//change page title
			changePageTitle("Search Products");
		});
		//prevent whole page reload
		return false;
	})
})