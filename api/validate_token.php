<?php
//headers
header('Allow-Control-Allow-Origin: http://new.jr/demo/');
header('Content-Type: application/json');
header('Allow-Control-Allow-Method: POST');
header('Allow-Control-Max-Age: 3600');
header('Allow-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

// required to decode jwt
include_once 'config/core.php';
include_once 'libs/php-jwt-master/src/BeforeValidException.php';
include_once 'libs/php-jwt-master/src/ExpiredException.php';
include_once 'libs/php-jwt-master/src/SignatureInvalidException.php';
include_once 'libs/php-jwt-master/src/JWT.php';
use \Firebase\JWT\JWT;

//get posted data
$data = json_decode(file_get_contents("php://input"));

//get jwt
$jwt = isset($data->jwt) ? $data->jwt : "";


if($jwt)
{
	//if decode succed, show user details
	try
	{
		//decode jwt
		$decoded = JWT::decode($jwt,$key,array('HS256'));

		http_response_code(200);

		echo json_encode(array(
			"message" => "Access Granted",
			"data" => $decoded->data
			)
		);
	}
	catch(Exception $e)
	{

		http_response_code(401);

		echo json_encode(array(
	        "message" => "Access denied.",
	        "error" => $e->getMessage()
	    ));

	}
}