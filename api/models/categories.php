<?php
	class Category{

		private $conn;
		private $table = "categories";

		public $id;
		public $name;
		public $description;
		public $created;


		function __construct($db)
		{
			$this->conn = $db;
		}


		public function readAll(){

			$query = "SELECT 
						id,
						name,
						description
					FROM
						".$this->table."
					ORDER BY
						name";

			$stmt = $this->conn->prepare($query);

			$stmt->execute();

			return $stmt;
		}
	}


?>