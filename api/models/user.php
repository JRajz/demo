<?php
	class User
	{
		private $conn;
		private $table='users';

		public $id;
		public $firstname;
		public $lastname;
		public $email;
		public $password;


		public function __construct($db)
		{
			$this->conn = $db;
		}

		//create method
		function create()
		{
			$query ="INSERT INTO 
							".$this->table."
						SET
							firstname=:firstname,
							lastname=:lastname,
							email=:email,
							password=:password";

			$stmt = $this->conn->prepare($query);

			$this->firstname = htmlspecialchars(strip_tags($this->firstname));
			$this->lastname = htmlspecialchars(strip_tags($this->lastname));
			$this->email = htmlspecialchars(strip_tags($this->email));
			$this->password = htmlspecialchars(strip_tags($this->password));
			
			$stmt->bindParam(':firstname',$this->firstname);
			$stmt->bindParam(':lastname',$this->lastname);
			$stmt->bindParam(':email',$this->email);
			
			$password_hash = password_hash($this->password, PASSWORD_BCRYPT);
			$stmt->bindParam(':password',$password_hash);

			if($stmt->execute()){
				return true;
			}
			return false;
		}

		//email exists method

		function emailExists()
		{
			$query = "SELECT 
							id, 
							firstname, 
							lastname, 
							password
						FROM 
							".$this->table."
						WHERE 
							email = ?
						LIMIT 0,1";

			//prepare query
			$stmt= $this->conn->prepare($query);

			$this->email = htmlspecialchars(strip_tags($this->email));

			$stmt->bindParam(1,$this->email);

			//execute query
			$stmt->execute();

			$num = $stmt->rowCount();

			if($num>0)
			{
				$row = $stmt->fetch(PDO::FETCH_ASSOC);

				$this->id = $row['id'];
				$this->firstname = $row['firstname'];
				$this->lastname = $row['lastname'];
				$this->password = $row['password'];
				
				//return true if email exists
				return true;
			}

			//return false if email does exist
			return fasle;
		}

		//update

		public function update(){

			$password_set=!empty($this->password) ? ", password = :password" : "";

			$query = "UPDATE ". $this->table ."
						SET
							firstname = :firstname,
							secondname = :secondname,
							email = :email
							($password_set}
							WHERE id = :id";

			$stmt = $this->prepare($query);

			//sanitize
			$this->firstname=htmlspecialchars(strip_tags($this->firstname));
			$this->lastname=htmlspecialchars(strip_tags($this->lastname));
			$this->email=htmlspecialchars(strip_tags($this->email));
			
			//bind values
			$stmt->bindParam(':firstname',$this->firstname);
			$stmt->bindParam(':lastname',$this->lastname);
			$stmt->bindParam(':email',$this->email);

			//hash password before saving to database
			if(!empty($this->password)){
				$this->password = htmlspecialchars(strip_tags($this->password));
				$password_hash = password_hash($this->password,PASSWORD_BCRYPT);
				$stmt->bindParam(":password",$password_hash);
			}

			//unique _id
			$stmt->bindParam(':id',$this->id);

			if($stmt->execute())
			{
				return true;
			}

			return false;
		}
	}

?>