<?php
	
	class Product{

		private $conn;
		private $table = "products";

		public $id;
		public $name;
		public $description;
		public $price;
		public $category_id;
		public $category_name;
		public $created;

		public function __construct($db)
		{
			$this->conn = $db;
		}

		//read product item
		public function read(){
			$query = "SELECT
						c.name as category_name,
						p.id,
						p.name,
						p.description,
						p.price,
						p.category_id,
						p.created
					FROM
						".$this->table." p
					LEFT JOIN 
						categories c 
							ON p.category_id = c.id
					ORDER BY
						p.created DESC";

			$stmt = $this->conn->prepare($query);

			$stmt->execute();

			return $stmt;
		}

		//create product
		public function create(){

			$query = "INSERT INTO ".$this->table."
						SET
							name=:name,
							price=:price,
							description=:description,
							category_id=:category_id,
							created=:created";
			//prepare query
			$stmt = $this->conn->prepare($query);

			$this->name = htmlspecialchars(strip_tags($this->name));
			$this->price = htmlspecialchars(strip_tags($this->price));
			$this->category_id = htmlspecialchars(strip_tags($this->category_id));
			$this->description = htmlspecialchars(strip_tags($this->description));
			$this->created = htmlspecialchars(strip_tags($this->created));

			//bind values
			$stmt->bindParam(":name",$this->name);
			$stmt->bindParam(":category_id",$this->category_id);
			$stmt->bindParam(":price",$this->price);
			$stmt->bindParam(":description",$this->description);
			$stmt->bindParam(":created",$this->created);

			if($stmt->execute()){
				return true;
			}

			return false;
		}


		//read one product item
		public function readOne(){
			$query = "SELECT
						c.name as category_name,
						p.id,
						p.name,
						p.description,
						p.price,
						p.category_id,
						p.created
					FROM
						".$this->table." p
					LEFT JOIN 
						categories c 
							ON p.category_id = c.id
					WHERE 
						p.id = ?
					LIMIT
						0,1";

			$stmt = $this->conn->prepare($query);

			$stmt->bindParam(1, $this->id);

			$stmt->execute();

			//get retireved row
			$row = $stmt->fetch(PDO::FETCH_ASSOC);

			//set value
			$this->name = $row['name'];
			$this->price = $row['price'];
			$this->category_id = $row['category_id'];
			$this->description = $row['description'];
			$this->category_name = $row['category_name'];			
		}


		//Update product
		public function update(){

			$query = "UPDATE 
							".$this->table."
						SET
							name=:name,
							price=:price,
							description=:description,
							category_id=:category_id
						WHERE
							id=:id";
			//prepare query
			$stmt = $this->conn->prepare($query);

			$this->name = htmlspecialchars(strip_tags($this->name));
			$this->price = htmlspecialchars(strip_tags($this->price));
			$this->category_id = htmlspecialchars(strip_tags($this->category_id));
			$this->description = htmlspecialchars(strip_tags($this->description));
			$this->id = htmlspecialchars(strip_tags($this->id));

			//bind values
			$stmt->bindParam(":name",$this->name);
			$stmt->bindParam(":category_id",$this->category_id);
			$stmt->bindParam(":price",$this->price);
			$stmt->bindParam(":description",$this->description);
			$stmt->bindParam(":id",$this->id);

			if($stmt->execute()){
				return true;
			}

			return false;
		}

		//Delete product
		public function delete(){

			$query = "DELETE FROM
							".$this->table."
						WHERE
							id = ?";

			$stmt = $this->conn->prepare($query);

			$this->id = htmlspecialchars(strip_tags($this->id));

			$stmt->bindParam(1, $this->id);

			if($stmt->execute()){
				return true;
			}
			return false;
		}


		//search product

		public function search($keywords){

			$query = "SELECT
			      			c.name as category_name,
			      			p.id,
			      			p.name,
			      			p.category_id,
			      			p.price,
			      			p.description,
			      			p.created
			      		FROM
			      			".$this->table." p
			      		LEFT JOIN
			      			categories c
			      				ON p.category_id = c.id
			      		WHERE
			      			p.name LIKE ? OR p.description LIKE ? OR c.name LIKE ?
			      		ORDER BY
			      			p.created DESC";

			$stmt = $this->conn->prepare($query);

			$keywords = htmlspecialchars(strip_tags($keywords));
			$keywords = "%{$keywords}%";

			//bind
			$stmt->bindParam(1, $keywords);
			$stmt->bindParam(2, $keywords);
			$stmt->bindParam(3, $keywords);

			$stmt->execute();

			return $stmt;
		}


		//paging
		public function readPaging($from_record_num,$records_per_page){
				
				$query = "SELECT
						c.name as category_name,
						p.id,
						p.name,
						p.description,
						p.price,
						p.category_id,
						p.created
					FROM
						".$this->table." p
					LEFT JOIN 
						categories c 
							ON p.category_id = c.id
					ORDER BY 
						p.created
					LIMIT ?, ?";

			$stmt = $this->conn->prepare($query);

			$stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
			$stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);

			$stmt->execute();

			return $stmt;
		}

		//used for paging products

		public function count(){
			$query = "SELECT 
							COUNT(*)
						as total_rows FROM 
							".$this->table."";

		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		return $row['total_rows'];

		}

	}
?>