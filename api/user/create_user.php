<?php

header('Allow-Control-Allow-Origin: http://new.jr/demo/');
header('Content-Type: application/json');
header('Allow-Control-Allow-Method: POST');
header('Allow-Control-Max-Age: 3600');
header('Allow-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');

include_once '../config/database.php';
include_once '../models/user.php';

//get database
$database = new Database();
$db = $database->getConnection();

//instantiate product object
$user = new User($db);

//submitted data will be here

$data = json_decode(file_get_contents("php://input"));
if($data->email){
	$user->firstname = $data->firstname;
	$user->lastname = $data->lastname;
	$user->email = $data->email;
	$user->password = $data->password;

	//use create method
	$user->create();
	if($user->create()){

		http_response_code(200);
		echo json_encode(array("message" => "User was created"));

	}
	else
	{
		http_response_code(400);
		echo json_encode(array("message" => "Unable to create user"));
	}
}
else
{
	http_response_code(403);
	echo json_encode(array("message" => "Unable to create user. Enter the details"));
}

?>