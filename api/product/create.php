<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age:3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization,X-Requsted-With");

include_once '../config/database.php';
include_once '../models/product.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

//get POSTED data

$data = json_decode(file_get_contents("php://input"));

//make sure data is not empty
if(
	!empty($data->name) &&
	!empty($data->description) &&
	!empty($data->price) &&
	!empty($data->category_id)
){

	//set product property value
	$product->name = $data->name;
	$product->price = $data->price;
	$product->description = $data->description;
	$product->category_id = $data->category_id;
	$product->created = date('Y-m-d H:i:s');

	if ($product->create()) {
		//set response
		http_response_code(201);
		echo json_encode(array("message"=>"Product was created"));

	}else{
		//set response unable to create product
		http_response_code(503);
		echo json_encode(array("message"=>"Unable to create product"));

	}

}
	//user data is incomplete
else{
	http_response_code(404);
	echo json_encode(array("message"=>"Unable to create product. Data incomplete"));
}

?>