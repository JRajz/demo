<?php

header("Access-Control-Allow-Access: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age:3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization,X-Requsted-With");

include_once '../config/database.php';
include_once '../models/product.php';

$database = new Database();
$db = $database->getConnection();

$product = new Product($db);

$data = json_decode(file_get_contents("php://input"));

$product->id = $data->id;

if($product->delete()){

	http_response_code(200);
	echo json_encode(array("message"=>"Product deleted"));

}else{
		//set response unable to delete product
		http_response_code(503);
		echo json_encode(array("message"=>"Unable to delete product"));

	}


?>